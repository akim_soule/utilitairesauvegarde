package Utilitaire;


import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.imageio.ImageIO;

public class Utilitaire {
	
	Object object = new Object();
	
	public void serialize() {
		try (BufferedOutputStream bos = new BufferedOutputStream(
				new FileOutputStream(new File("sauvegarde/serial.ser")));
				ObjectOutputStream oos = new ObjectOutputStream(bos)) {
			oos.writeObject(object);
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
			// e.getSuppressed();
		}
	}

	/**
	 * Un SuppressWarning parce que lors du cast de l'objet retObj en
	 * ArbreBinaireDerive<Object>, il n'y a aucune garanti que l'objet retObj soit
	 * un arbre d'objets.
	 */
	
	public void deserialize() {
		Object retObj = null;
		try (BufferedInputStream bos = new BufferedInputStream(new FileInputStream(new File("sauvegarde/serial.ser")));
				ObjectInputStream ois = new ObjectInputStream(bos)) {
			retObj = ois.readObject();
			object = (Object) retObj;
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			// e.getSuppressed();
		}
	}
	
	public void sauvegarderImage() {
		
		try {
			BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
			ImageIO.write(image, "png", new File("/screenshot.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HeadlessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
